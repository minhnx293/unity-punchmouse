﻿using UnityEngine;
using System.Collections;

public class DestroybyBoundary : MonoBehaviour {

	void OnTriggerExit(Collider other) {
		// Destroy everything that leaves the trigger
		if (!other.gameObject.CompareTag ("Boundary")) {
			Destroy(other.gameObject);
		}
	}
}
