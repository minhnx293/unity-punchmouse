﻿using UnityEngine;
using System.Collections;

//move sin
public class MoveMouse1 : MonoBehaviour {

	public float speed;
	public float amplitude = 26.0f;
	float t = 15f;

	private Vector3 pos;

	private Rigidbody rigibody;

	void Start (){
		rigibody = GetComponent<Rigidbody> ();
		rigibody.velocity = transform.forward * speed;
		pos = transform.position;
	}

	void FixedUpdate(){
		pos.x = amplitude * Mathf.Sin(t);
		pos.y = transform.position.y;
		pos.z += -speed * Time.deltaTime;
			t += Time.deltaTime;
		transform.position = pos;
	}

	void SetPos(){
		//set pos..
	}
}
