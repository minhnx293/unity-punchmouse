﻿using UnityEngine;
using System.Collections;

// move with speed up 
public class MoveMouse2 : MonoBehaviour {

	public float speed;
	public float deltaSpeed;

	private GameController gameController;
	private float deltaX;
	private float deltaZ;
	private Vector3 pos;
	private Rigidbody rigibody;

	// Use this for initialization
	void Start ()
	{
		GameObject gameControllerObject = GameObject.FindWithTag ("GameController");
		if (gameControllerObject != null) {
			gameController = gameControllerObject.GetComponent<GameController> ();

			if (gameController.cake != null) {
				deltaX = Mathf.Abs (transform.position.x - gameController.cake.transform.position.x);
				deltaZ = Mathf.Abs (transform.position.z - gameController.cake.transform.position.z);
			}
		} else {
			Debug.Log ("khong tim thay 'GameController'");
			//		Debug.Log ("minhnx293::" + gameController.cake.transform.position.z);
			deltaX = transform.position.x ;
			deltaZ = transform.position.z;
		}

		rigibody = GetComponent<Rigidbody> ();
		rigibody.velocity = transform.forward * speed;
		pos = transform.position;
//		Debug.Log ("minhnx293::" + gameController.cake.transform.position.z);
		deltaX = Mathf.Abs (transform.position.x - gameController.cake.transform.position.x);
		deltaZ = Mathf.Abs (transform.position.z - gameController.cake.transform.position.z);
	}

	void FixedUpdate()
	{
		pos.x = deltaX / deltaZ * transform.position.z;
		pos.y = transform.position.y;
		pos.z += - (deltaSpeed + speed) * Time.deltaTime;
		transform.position = pos;
	}
}