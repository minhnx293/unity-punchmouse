﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.Generic;

public class GameController : MonoBehaviour {

	public Camera camera;

	public GameObject cake;
	public MoveMouse1 mouse1;
	public GameObject mouse2;
	public Vector3 spawnValue;
	public int mouse1Count;
	public int mouse2Count;
	public float spawnWait;
	public float startWait;
	public float waitWait;

	public Text scoreText;
	public Text restartText;
	public Text gameOverText;

	public List<MoveMouse1> arrMouse1 = new List<MoveMouse1>();
	public List<MoveMouse2> arrMouse2 = new List<MoveMouse2>();
	public AudioClip audioClip;

	private bool gameOver;
	private bool restart;
	private int score;
	//	public GameObject mouse;

	void Start ()	{
		gameOver = false;
		restart = false;
		restartText.text = "";
		gameOverText.text = "";
		score = 0;

		//check music play/stop
		if (PlayerPrefs.GetInt ("MusicState") == 0) {
			AudioSource audio = GetComponent<AudioSource> ();
			audio.Stop();
		}

		StartCoroutine (SpawnWaves1 ());
		StartCoroutine (SpawnWaves2 ());
	}

	void Update ()	{
		if (restart) {
			if (Input.GetKeyDown (KeyCode.R)) {
				Application.LoadLevel (Application.loadedLevel);
			}
		}

		//add check click to mouse
		if(Input.GetMouseButtonDown(0)){
			Ray ray = camera.ScreenPointToRay (Input.mousePosition);
			RaycastHit hit;
			if (Physics.Raycast (ray, out hit)) {
//				Debug.LogError (hit.transform.gameObject.name);
				if (hit.transform.gameObject.CompareTag ("Mouse1")) {
					Destroy (hit.transform.gameObject);
					AddScore (1);
				}else if (hit.transform.gameObject.CompareTag ("Mouse2")) {
					Destroy (hit.transform.gameObject);
					AddScore (2);
				}
			}
		}
	}
		
	IEnumerator SpawnWaves1 ()	{
		yield return new  WaitForSeconds (startWait);//first wait
		while (true) {
			for (int i = 0; i < mouse1Count; i++) {
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValue.x, spawnValue.x), spawnValue.y, spawnValue.z);
				Quaternion spawnRotation = Quaternion.identity;
//				Instantiate (mouse1, spawnPosition, spawnRotation);
//				Missile missileCopy = Instantiate<Missile>(missile);
				MoveMouse1 obj = Instantiate<MoveMouse1>(mouse1); // (mouse1, spawnPosition, spawnRotation) as MoveMouse1;
				arrMouse1.Add (obj);
				Debug.Log ("LOL:::"+arrMouse1.Count);
//				Instantiate
				yield return new  WaitForSeconds (spawnWait);
			}
			yield return new  WaitForSeconds (waitWait);

			if (gameOver) {
				restart = true;
				break;
			}
		}
	}

	IEnumerator SpawnWaves2 ()	{
		yield return new  WaitForSeconds (startWait);//first wait
		while (true) {
			for (int i = 0; i < mouse2Count; i++) {
				Vector3 spawnPosition = new Vector3 (Random.Range (-spawnValue.x, spawnValue.x), spawnValue.y, spawnValue.z);
				Quaternion spawnRotation = Quaternion.identity;
				Instantiate (mouse2, spawnPosition, spawnRotation);
				yield return new  WaitForSeconds (spawnWait);
			}
			yield return new  WaitForSeconds (waitWait);

			if (gameOver) {
				restartText.text = "Press 'R' for Restart";
				restart = true;
				break;
			}
		}
	}

	public void AddScore(int newScoreValue){
		score += newScoreValue;
		UpdateScore ();
	}

	void UpdateScore(){
		scoreText.text = "Score: " + score;
	}

	public void GameOver(){
		gameOverText.text = "Game Over";
		gameOver = true;
	}
}
