﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using DG.Tweening;
using UnityEngine.SceneManagement;

public class MenuGameController : MonoBehaviour {

	public Button btnPlay;
	public Button btnMusic;
	public Image imgFade;
	public float duration;
	public GUITexture musicOff;

	private bool isMusic;
	// Use this for initialization
	void Start () {
		isMusic = true;
		PlayerPrefs.SetInt ("MusicState", 1); //stop
		Button btnPlayClick = btnPlay.GetComponent<Button>	();
		btnPlayClick.onClick.AddListener (btnPlayGameOnclick);

		Button btnMusicClick = btnMusic.GetComponent<Button>();
		btnMusicClick.onClick.AddListener (btnMusicOnclick);
	}
	
	// Update is called once per frame
	void Update () {
		
	}

	void btnPlayGameOnclick(){
//		Debug.LogError("start game:: btn play is clicked");
		StartCoroutine (StartFade ());
	}
	void btnMusicOnclick(){
//		Debug.LogError("start game:: btnMusicOnclick");
		if (isMusic) {//dang phat
			PlayerPrefs.SetInt ("MusicState", 0); //stop
		} else {
			PlayerPrefs.SetInt ("MusicState", 1); //play music
		}
	}


	IEnumerator StartFade(){
		// đợi Scene load xong.
		yield return new WaitForEndOfFrame();
		Fade();
	}

	void Fade(){
		imgFade.CrossFadeAlpha (1, duration, true);
		SceneManager.LoadScene("GameScene");
	}
}
